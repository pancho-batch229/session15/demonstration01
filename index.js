// js comments crtl+/
// Multi-lined ctrl+shift+/
	/*
	Open
	Up
	Your
	Eyes
	*/

// STATEMENTS
// Programming Instructions that we tell the computer to perform.

// SYNTAX
// Set of rules that describe how statements must be constructed.

// alert("hello you!");

// This is a statement
console.log("Hello World!");
console.log(  "      Hello World!     "  );
console.
log 
(
	"Hello Again"
	)

// [SECTION] Variables
// Used as containers or storage

// Declaring variables - tells device that a variable name is created and ready to store data.

// Syntax - let/const variableName;

// "let" is a keyword that is usually used to declare a variable
// variables should start with lowercase - camel casing
// const variable use const keyword
// variable names should be descrptive and comprehensive

let myVariable;
let hello;

console.log(myVariable);
console.log(hello);


// = initialization 

let firstName = "Ashley";

let productName = "desktop computer";
console.log(productName);

let productPrice = 29013;
console.log(productPrice);

// re-assigning a value to a variable.
productPrice = 25000;
console.log(productPrice);

let friend = "Kate";
friend = "Jane";
console.log(friend);

let supplier;
supplier = "John Smith";
console.log(supplier);

// let variables can be changed, re-assigned new values.
// const values cannot be changed.

const hoursInAday = 24;
console.log(hoursInAday);

//local/global scale variable

let outerVariable = "hi";

{
	let innerVariable = "hello";
	console.log(innerVariable);
	console.log(outerVariable);
}

console.log(outerVariable);
/*
inner values cannot be accessed
console.log(innerVariable);
*/

// MULTIPLE VARIABLE DECLARATION
// Multiple variables may be declared in one line
// Convenient and it's easier to read the code.

/*
let productCode = "DC017"
let productBand = "Dell"
*/

let productCode = "DC017", productBrand = "Dell";

console.log(productCode, productBrand);

/*
const let = "hello";

let is a reserved keyword and reserved keywords cannot be used
*/

// [SECTION] Data Types

// Strings - are series of characters that creates a word.

let country = "Philippines";
let province = "Metro Manila";


// Concatenating String, uses + symbol to combine
// Combining string values

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// escape character (\)

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";

console.log(message);
message = 'John\'s employee went home early';
console.log(message);

// Numbers
// Integers or Whole

let headcount = 26;

// Decimal or Fractions
let grade = 90.2;
console.log(grade);

// Exponential Notation
let plantDistance = 2e10;
console.log(plantDistance);

// combining Text and Strings
console.log("John's lst grade last quarter is " + grade);

// Boolean
// we have 2 boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays is a special data type
// store multiple values with the same data types

// Syntax -> let/const arrayName = [elementA, elementB, ....]

let grades = [123, 4324, 154];
console.log(grades);

/*
not the best use 

let details = ["John", "Smith", 32, true];
console.log(details);
*/

// Objects
// Hold properties that describe the variable
// Syntax -> let/const objectName = {propertyA: value, propertyB: value}

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["092103921", "192301832"],
	address: {
		houseNumber: "3213",
		city: "Manila",
	}
}

console.log(person);

let myGrades = {
	firstGrading: 32.4,
	secondGrading: 12.1,
	thirdGrading: 34.2,
	fourthGrading: 95.1
}
console.log(myGrades);

// checking of data types

console.log(typeof grades);

const anime = ["one piece", "one punch man", "attack on titan"];
anime[0] = "dragon ball";


console.log(anime);

spouse = null;
